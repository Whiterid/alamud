# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class OuvrirAvecEvent(Event3):
    NAME = "ouvrir-avec"

    def perform(self):
        if self.object2.has_prop("key-for-bierre"):
            if self.object.has_prop("ferme"):
                self.object.remove_prop("ferme")
                self.inform("ouvrir-avec")
            else:
                self.ouvriravec_failed()
        else:
            self.ouvriravec_failed()


    def ouvriravec_failed(self):
        self.fail()
        self.inform("ouvrir-avec.failed")
